import org.serviio.library.metadata.*
import org.serviio.library.online.*
import groovy.json.JsonSlurper

/**
 * WebResource extractor plugin for www.allocine.fr
 *
 * @author Illico
 * @version 1.2
 *
 */
class AlloCine extends WebResourceUrlExtractor {

	int VERSION = 12
	final LOGO_URL = "http://www.1jour1app.com/images/LogoAllocine.png"
	final VALID_FEED_URL = 'http://www.allocine.fr.*'
	final PARTNER_KEY = "aXBhZC12MQ"
	final OUTPUT_FORMAT = "json"
	final MEDIA_FORMAT = "mp4"

	final URL_BASE = "http://api.allocine.fr/rest/v3/"
	//final USER_AGENT = "Dalvik/1.6.0 (Linux; U; Android 4.2.2; Nexus 4 Build/JDQ39E)"
	//final ALLOCINE_PARTNER_KEY = "100043982026"
	//final ALLOCINE_SECRET_KEY = "29d185d98c984a359e6e6f26a0474269"

	String getExtractorName() {
		return "Allocine.fr"
	}

	int getVersion() {
		return VERSION
	}

	boolean extractorMatches(URL feedUrl) {
		return feedUrl ==~ VALID_FEED_URL
	}

	WebResourceContainer extractItems(URL resourceUrl, int maxItems) {
		String WebResourceTitle = "Allocine.fr"
		String WebResourceThumbnailUrl = "http://www.1jour1app.com/images/LogoAllocine.png"
		// API parameters
		// http://api.allocine.fr/rest/v3/movielist?partner=aXBhZC12MQ&count=25&filter=nowshowing&page=1&order=datedesc&format=json
		// http://api.allocine.fr/rest/v3/movielist?partner=aXBhZC12MQ&count=25&filter=comingsoon&page=1&order=toprank&format=json
		// http://api.allocine.fr/rest/v3/movielist?partner=aXBhZC12MQ&count=25&filter=comingsoon&page=1&order=dateasc&format=json
		String method = "movielist"
		String type_filter = "comingsoon" //nowshowing , comingsoon,
		String order = "toprank" //(type_filter == "nowshowing") ? "datedesc" : "dateasc"; // datedesc, dateasc, theatercount, toprank
		String count = ( maxItems == -1 ) ? "30" : maxItems.toString();
		String page = "1"
		String striptags = 'synopsis'
		String query_string = 'partner='+PARTNER_KEY+'&filter='+type_filter+'&format='+OUTPUT_FORMAT+'&order='+order+'&count='+count+'&page='+page+'&striptags='+striptags
		String MovieListUrl = URL_BASE + method +'?'+ query_string
		loginfo(MovieListUrl);
		def MovieListJson = new JsonSlurper().parseText((new URL(MovieListUrl)).getText("utf-8"))
		List<WebResourceItem> items = []
		//MovieListJson.feed.movie.each { value -> if ( value.containsKey("trailer")) { loginfo("Titre: "+value.title+",trailerCode: "+value.trailer.code+",thumb: "+value.poster.href)                                                                   } }
		MovieListJson.feed.movie.each { value -> if ( value.containsKey("trailer")) { WebResourceItem item = new WebResourceItem(title: value.title, additionalInfo: ['trailerCode': value.trailer.code, 'thumbUrl':value.poster.href]); items << item; } }
		return new WebResourceContainer(title: WebResourceTitle, thumbnailUrl: WebResourceThumbnailUrl, items: items)
	}

	ContentURLContainer extractUrl(WebResourceItem item, PreferredQuality requestedQuality) {
		def videoUrl
		// API parameters
		// http://api.allocine.fr/rest/v3/media?partner=aXBhZC12MQ&code=19425786&profile=large&mediafmt=mp4-lc&format=json
		String method = "media"
		String profile ="small" // small, medium, large
		String mediafmt ="mp4-hip"
		/*    flv : FLV / H.264
		 mp4-lc : MP4 / H.264 Baseline Profile, Low Complexity, with splashscreen
		 mp4-hip : H264 High Profile, with splashscreen
		 mp4-archive : MP4 / H.264 High Profile, for archive
		 mpeg2-theater : MPEG-2 720p
		 mpeg2 : MPEG-2 Main Profile
		 */
		String query_string = 'partner='+PARTNER_KEY+'&code='+item.getAdditionalInfo()['trailerCode']+'&profile='+profile+'&mediafmt='+mediafmt+'&format='+OUTPUT_FORMAT
		String TrailerInfoUrl = URL_BASE + method + '?'+query_string
		loginfo(TrailerInfoUrl);
		def TrailerInfojson = new JsonSlurper().parseText((new URL(TrailerInfoUrl)).getText("utf-8"))
		//TrailerInfojson.media.rendition.each { value -> loginfo("videoMaxBitRate"+value.videoMaxBitRate+", href:"+value.href) }
		def list = []
		TrailerInfojson.media.rendition.each { value -> list << ['url': value.href, 'bitrate': value.videoMaxBitRate.toInteger()] }
		list = list.sort{ it.bitrate }.reverse()
		/* Depending on the quality requested, return different url's */
		if (requestedQuality == PreferredQuality.HIGH) {
			videoUrl = list[0]
		} else if (requestedQuality == PreferredQuality.MEDIUM) {
			videoUrl = list.size() > 1 ? list[1] : list[0]
		} else if (requestedQuality == PreferredQuality.LOW) {
			videoUrl = list.size() > 2 ? list[2] : list.size() > 1 ? list[1] : list[0]
		}
		loginfo("VideoUrl: "+videoUrl.url)
		def cacheKey = getClass().getName() + "_${requestedQuality}"
		return new ContentURLContainer(fileType: MediaFileType.VIDEO, contentUrl: videoUrl.url, thumbnailUrl: item.getAdditionalInfo()['thumbUrl'], live: false, cacheKey: cacheKey)
	}

	private String loginfo(String text) {
		log(text);
		println(text);
	}

	static WebResourceContainer testURL(String url, int itemCount = 2) {
		AlloCine extractor = new AlloCine();
		URL resourceUrl = new URL(url);
		println "getExtractorName : " + extractor.getExtractorName();
		println "getVersion : " + extractor.getVersion();
		assert extractor.extractorMatches(resourceUrl), 'Url doesn\'t match for this WebResource plugin'
		println "extractorMatches : " + extractor.extractorMatches(resourceUrl);
		WebResourceContainer container = extractor.extractItems(resourceUrl, itemCount);
		assert container != null, 'Container is empty'
		assert container.items != null, 'Container contains no items'
		//assert container.items.size() == itemCount, 'Amount of items is invalid. Expected was ' + itemCount + ', result was ' + container.items.size()
		println "extractItems : " + container.items.size()
		println "***** HIGH *****";extractor.extractUrl(container.getItems()[1], PreferredQuality.HIGH);
		println "**** MEDIUM ****";extractor.extractUrl(container.getItems()[1], PreferredQuality.MEDIUM);
		println "***** LOW ******";extractor.extractUrl(container.getItems()[1], PreferredQuality.LOW);
		return container
	}
	/*********************************************
	 * MAIN
	 *********************************************/ 
	static void main(args) {
		testURL("http://www.allocine.fr",-1)
	}
}