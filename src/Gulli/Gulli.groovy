import groovy.util.Node

import java.util.List

import org.serviio.library.metadata.*
import org.serviio.library.online.*
import org.serviio.util.*

/**
 * Content URL extractor plugin for Gulli
 * 
 * http://replay.gulli.fr/layout/set/rss/RSS
 *
 * @author Illico
 *
 * @version 1.0
 */
class Gulli extends FeedItemUrlExtractor {

	final VALID_FEED_URL = '(^http://.*gulli.*RSS$)'
	final VALID_THUMB_URL = '.*og:image.*'

	String getExtractorName() {
		return 'Gulli'
	}

	boolean extractorMatches(URL feedUrl) {
		return feedUrl ==~ VALID_FEED_URL
	}

	ContentURLContainer extractUrl(Map links, PreferredQuality requestedQuality) {
		def linkUrl = links.alternate != null ? links.alternate : links.default
		// GET Video Id
		//log('linkUrl: '+ linkUrl.toString())
		def linkParts0 = linkUrl.toString().split('/')
		def VideoId = linkParts0[7].toString()
		log('VideoId: '+ VideoId)

		// Open the HTML page first
		def webPage = linkUrl.getText()
		// Thumbnail ex: <meta property="og:image" content="http://cdn-gulli.ladmedia.fr/var/storage/imports/replay/images_programme/leonard.jpg" />
		def matcher1 = webPage =~ VALID_THUMB_URL
		assert matcher1 != null
		def linkParts1 = matcher1[0].toString().split('"')
		def ThumbnailUrl = linkParts1[3].toString()
		log('ThumbnailUrl: '+ ThumbnailUrl)

		// GET Video Info Url
		// http://cdn-gulli.ladmedia.fr/var/storage/imports/replay/smil/VOD277053.smil
		// 		<video region="video" src="9722VOD259.mp4" clipBegin="smtpe=00:00:00:00" clipEnd="smtpe=00:21:41:01" readIndex="1" id="VOD277053" fit="fill"/>
		String VideoInfoUrl = "http://cdn-gulli.ladmedia.fr/var/storage/imports/replay/smil/${VideoId}.smil"
		def rssNodes = new XmlParser().parse(VideoInfoUrl)
		def VideoPath = rssNodes.body.par.video.'@src'.text()
		log('VideoPath: '+ VideoPath.toString())
		// ex: rtmp://stream2.lgdf.yacast.net/gulli_replay app=gulli_replay swfUrl=http://cdn-gulli.ladmedia.fr/extension/replay/design/replay/templates/replay/images/playerCuePoint4.swf playpath=mp4:9722VOD259.mp4 swfVfy=1
		def RTMP_playpath = VideoPath
		def RTMP_Server = 'stream2.lgdf.yacast.net:80/gulli_replay'
		def RTMP_App = 'gulli_replay'
		def RTMP_swfUrl = 'http://cdn-gulli.ladmedia.fr/extension/replay/design/replay/templates/replay/images/playerCuePoint4.swf'
		def contentUrl = "rtmp://" + RTMP_Server + " app=" + RTMP_App + " swfUrl=" + RTMP_swfUrl + " playpath=mp4:" + RTMP_playpath + " swfVfy=1"
		log('ContentUrl(rtmp): ' + contentUrl)
		return new ContentURLContainer(contentUrl: contentUrl, thumbnailUrl: ThumbnailUrl)
	}

	static void main(args) {
		// this is just to test
		Gulli extractor = new Gulli()

		assert extractor.extractorMatches( new URL("http://replay.gulli.fr/layout/set/rss/RSS") )
		assert !extractor.extractorMatches( new URL("http://google.com/feeds/api/standardfeeds/top_rated?time=today") )
		// http://replay.gulli.fr/replay/video/dessins-animes/beyblade_metal_masters/VOD277053
		// http://replay.gulli.fr/replay/video/dessins-animes/il_etait_une_fois_lavie/VOD248042
		Map videoLinks = ['default': new URL("http://replay.gulli.fr/replay/video/dessins-animes/beyblade_metal_masters/VOD277053")]
		ContentURLContainer result = extractor.extractUrl(videoLinks, PreferredQuality.MEDIUM)
		println "Result: $result"
	}
}