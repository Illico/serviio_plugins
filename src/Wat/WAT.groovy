import groovy.util.Node
import groovy.json.*

import org.serviio.library.metadata.*
import org.serviio.library.online.*
import org.serviio.util.*

import java.security.MessageDigest

/**
 * Content URL extractor plugin for WAT
 *
 * List of RSS : http://www.wat.tv/rssmap
 * RSS sample : http://www.wat.tv/rss/theme/enfant-jeunesse-tv/buzz
 *
 * @author Illico
 *
 * @version 1.3
 *
 */
class WAT extends FeedItemUrlExtractor {

	final VALID_FEED_URL = '^http://www.wat.tv/rss/.*$'
	final DOMAIN = "www.wat.tv"
	final URLPLAYER  = "http://www.wat.tv/images/v40/PlayerWat.swf?revision=4.1.017.23"
	final URLPLAYER1 = "http://www.wat.tv/images/v40/loaderexport.swf"
	int VERSION = 13

	String getExtractorName() {
		return getClass().getName()
	}

	int getVersion() {
		return VERSION
	}

	boolean extractorMatches(URL feedUrl) {
		return feedUrl ==~ VALID_FEED_URL
	}

	ContentURLContainer extractUrl(Map links, PreferredQuality requestedQuality) {
		def linkUrl = links.alternate != null ? links.alternate : links.default
		println "Item link : $linkUrl"
		def webPage = linkUrl.getText()
		// ThumbnailUrl
		def matcher1 = webPage =~ '<meta property="og:image" content="(.*?)"'
		assert matcher1 != null, log("Error: Cette page ne contient pas de vignette");
		def ThumbnailUrl = matcher1[0][1].toString()
		// Video Url
		def matcher2 = webPage =~ '<meta property="og:url" content="(.*?)"'
		assert matcher2 != null, log("Error(url): Cette page ne contient pas d'Url");
		def VideoUrl = matcher2[0][1].toString()
		// Video Id
		def matcher3 = webPage =~ 'WATPlayer.showPlayer.*,url : ".*?nIc0K11(\\d+)"'
		assert matcher3 != null, log("Error(VideoId): Cette page ne contient pas d'Url");
		def VideoId= matcher3[0][1].toString()

		// GET Video Info
		String VideoInfoUrl = "http://" + DOMAIN + "/interface/contentv3/${VideoId}"
		log("VideoInfoUrl : $VideoInfoUrl"); println "VideoInfoUrl : $VideoInfoUrl"
		def VideoInfoPage = new URL(VideoInfoUrl).getText()
		def slurper = new JsonSlurper()
		def result = slurper.parseText(VideoInfoPage)
		println " id=" + result.media.id + "\n uid=" + result.media.uid + "\n files.id=" + result.media.files[0].id + "\n files.hasHD=" + result.media.files[0].hasHD
		// fileId
		def fileId = result.media.files[0].id
		// GET token in MD5
		MessageDigest digest = MessageDigest.getInstance("MD5")
		def watURL
		if ( ( result.media.files[0].hasHD.toString() == "true" ) && (requestedQuality == PreferredQuality.HIGH)){
			log("IsHD : Yes"); println "IsHD : Yes"
			watURL = "/webhd/" + fileId
		} else {
			log("IsHD : No"); println "IsHD : No"
			watURL = "/web/" + fileId
		}
		def key = "9b673b13fa4682ed14c3cfa5af5310274b514c4133e9b3a81e6e3aba00912564"
		def dthex = String.format("%X", (System.currentTimeMillis()/1000).toInteger())
		digest.update((key + watURL + dthex).bytes)
		BigInteger big = new BigInteger(1,digest.digest())
		String md5 = big.toString(16).padLeft(32,"0")
		def token = md5 + "/" + dthex
		// GET Content Url
		def url4videoPath
		def contentUrl
		if ( ( result.media.files[0].hasHD.toString() == "true" ) && (requestedQuality == PreferredQuality.HIGH)){
			url4videoPath = "http://" + DOMAIN + "/get/webhd/" + fileId + "?token=" + token + "&domain=" + DOMAIN + "&context=swfpu&country=FR&getURL=1&version=LNX%2011,1,102,55"
		} else {
			url4videoPath = "http://" + DOMAIN + "/get/web/" + fileId + "?token=" + token + "&domain=" + DOMAIN + "&context=swfpu&country=FR&getURL=1&version=LNX%2011,1,102,55"
		}
		log("url4videoPath : $url4videoPath"); println "url4videoPath : $url4videoPath"
		def rtmpUrlpage = (new URL(url4videoPath)).getText()
		if ( rtmpUrlpage.startsWith('rtmp') ) {
			def rtmpUrlmatcher = rtmpUrlpage.split(":")
			def rtmpprotocol = rtmpUrlmatcher[0].split(",")
			def rtmpServermatcher = rtmpUrlpage.split("/")
			def RTMP_Server = rtmpServermatcher[2] + ":80/" + rtmpServermatcher[3] // wsel3.wat.tv, wske.wat.tv
			contentUrl = rtmpprotocol[0] + "://" + RTMP_Server + " app=" + rtmpServermatcher[3] + " swfUrl=" + URLPLAYER + " playpath=mp4:" + rtmpUrlmatcher[2] + " swfVfy=1"
			println "contentUrl(rtmp) : " + contentUrl
		} else {
			contentUrl = rtmpUrlpage
			println "contentUrl(http) : " + contentUrl
		}
		def cacheKey = getClass().getName() + "_${fileId}_${requestedQuality}"
		return new ContentURLContainer(contentUrl: contentUrl, thumbnailUrl: ThumbnailUrl, expiresImmediately: true, cacheKey : cacheKey)
	}

	static void main(args) {
		// this is just to test
		WAT extractor = new WAT()
		Map videoLinks = ['default': new URL("http://www.wat.tv/video/ferme-barbapapa-5gac9_55g3x_.html")]

		println "Name : " + extractor.getExtractorName();
		println "Version : " + extractor.getVersion();
		println "TestMatch : " + extractor.extractorMatches( new URL("http://www.wat.tv/rss/theme/enfant-jeunesse-tv/buzz"));
		println "**** HIGH ****";extractor.extractUrl(videoLinks, PreferredQuality.HIGH);
		println "**** MEDIUM ****";extractor.extractUrl(videoLinks, PreferredQuality.MEDIUM);
		println "**** LOW ****";extractor.extractUrl(videoLinks, PreferredQuality.LOW);
	}
}